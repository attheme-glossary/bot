use attheme_glossary_parser::glossary::{Contents, ParagraphItem};
use std::fmt::Write;

fn render_paragraph(
    buffer: &mut String,
    language: &str,
    paragraph: &[ParagraphItem],
) {
    paragraph.iter().for_each(|item| match item {
        ParagraphItem::LineBreak => buffer.push('\n'),
        ParagraphItem::Text(text) => buffer.push_str(text),
        ParagraphItem::Bold(text) => {
            buffer.push_str("**"); // leaving those for clarity
            render_paragraph(buffer, language, text);
            buffer.push_str("**");
        }
        ParagraphItem::Italic(text) => {
            buffer.push('_');
            render_paragraph(buffer, language, text);
            buffer.push('_');
        }
        ParagraphItem::Strikethrough(text) => {
            // Hopefully that's going to become the syntax
            buffer.push_str("~~");
            render_paragraph(buffer, language, text);
            buffer.push_str("~~");
        }
        ParagraphItem::Monospace(text) => {
            write!(buffer, "`{}`", text).unwrap();
        }
        ParagraphItem::Link { text, .. } => {
            render_paragraph(buffer, language, &text);
        }
    })
}

pub fn render_contents(
    buffer: &mut String,
    language: &str,
    contents: &[Contents],
) {
    contents.iter().for_each(|content| {
        match content {
            Contents::Paragraph(paragraph) => {
                render_paragraph(buffer, language, &paragraph.items);
                buffer.push_str("\n\n");
            }
            Contents::List { start, items } => {
                items.iter().enumerate().for_each(|(index, item)| {
                    if let Some(start) = start {
                        write!(buffer, "{}. ", start + index).unwrap();
                    } else {
                        buffer.push_str("— ");
                    }

                    render_contents(buffer, language, &item);
                })
            }
            Contents::Variable { .. }
            | Contents::Image { .. }
            | Contents::Subsection(..) => {
                // ignored on purpose
            }
        }
    });
}
