use attheme_glossary_parser::glossary::{Contents, ParagraphItem};
use std::fmt::Write;

const ESCAPED_CHARACTERS: [char; 5] = ['_', '*', '`', '[', ']'];

fn escape(text: &str) -> String {
    let mut new_string = String::with_capacity(text.len());

    text.chars().for_each(|symbol| {
        if ESCAPED_CHARACTERS.contains(&symbol) {
            new_string.push('\\');
        }

        new_string.push(symbol);
    });

    new_string
}

fn escape_mono(text: &str) -> String {
    text.replace("`", "\\`")
}

pub fn create_target(title: &str) -> String {
    title.to_lowercase().replace(" ", "-")
}

pub fn render_paragraph(
    buffer: &mut String,
    language: &str,
    paragraph: &[ParagraphItem],
) {
    paragraph.iter().for_each(|item| match item {
        ParagraphItem::LineBreak => buffer.push('\n'),
        ParagraphItem::Text(text) => buffer.push_str(&escape(text)),
        ParagraphItem::Bold(text) => {
            buffer.push('*');
            render_paragraph(buffer, language, text);
            buffer.push('*');
        }
        ParagraphItem::Italic(text) => {
            buffer.push('_');
            render_paragraph(buffer, language, text);
            buffer.push('_');
        }
        ParagraphItem::Strikethrough(text) => {
            // Hopefully that's going to become the syntax
            buffer.push('~');
            render_paragraph(buffer, language, text);
            buffer.push('~');
        }
        ParagraphItem::Monospace(text) => {
            write!(buffer, "`{}`", escape_mono(text)).unwrap();
        }
        ParagraphItem::Link { text, url } => {
            let url = if url.starts_with('#') {
                format!(
                    "https://attheme-glossary.snejugal.ru/{}{}",
                    language, url
                )
            } else {
                url.to_string()
            };

            buffer.push('[');
            render_paragraph(buffer, language, &text);
            write!(buffer, "]({})", url).unwrap();
        }
    })
}

fn render_contents(buffer: &mut String, language: &str, contents: &[Contents]) {
    contents.iter().for_each(|content| {
        match content {
            Contents::Paragraph(paragraph) => {
                render_paragraph(buffer, language, &paragraph.items);
                buffer.push_str("\n\n");
            }
            Contents::List { start, items } => {
                items.iter().enumerate().for_each(|(index, item)| {
                    if let Some(start) = start {
                        write!(buffer, "{}. ", start + index).unwrap();
                    } else {
                        buffer.push_str("— ");
                    }

                    render_contents(buffer, language, &item);
                })
            }
            Contents::Variable { .. }
            | Contents::Image { .. }
            | Contents::Subsection(..) => {
                // ignored on purpose
            }
        }
    });
}

pub fn render_section(
    buffer: &mut String,
    language: &str,
    prefix: &str,
    title: &str,
    contents: &[Contents],
    // if it is, the website renders it without its heading, and the bot
    // should not render an anchor.
    is_first_section: bool,
) {
    let target = if is_first_section {
        String::new()
    } else {
        create_target(title)
    };
    write!(
        buffer,
        "[{title}](https://attheme-glossary.snejugal.ru/{language}#{prefix}{target})\
         \n\n",
        title = escape(title),
        language = language,
        prefix = prefix,
        target = target,
    )
    .unwrap();

    render_contents(buffer, language, contents);
}
