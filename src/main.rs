mod bot;
mod index;
mod parser;

use index::IndexedGlossaries;
use parser::Glossaries;

#[tokio::main]
async fn main() {
    let glossaries = parser::parse();
    let indexed_glossaries = index::index_glossaries(&glossaries);

    bot::run(glossaries, indexed_glossaries).await;
}
