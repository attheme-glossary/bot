use crate::Glossaries;
use std::{collections::HashMap, io::Write, path::Path};
use tantivy::{
    schema::{
        Field, IndexRecordOption, Schema, TextFieldIndexing, TextOptions,
    },
    tokenizer::{Language, LowerCaser, SimpleTokenizer, Stemmer, Tokenizer},
    Document, Index,
};

pub struct IndexedGlossaries {
    pub title_field: Field,
    pub body_field: Field,
    pub position_field: Field,
    pub glossaries: HashMap<String, Index>,
}

pub fn get_language(language: &str) -> Language {
    match language {
        "ar" => Language::Arabic,
        "da" => Language::Danish,
        "nl" => Language::Dutch,
        "en" => Language::English,
        "fi" => Language::Finnish,
        "fr" => Language::French,
        "de" => Language::German,
        "gr" => Language::Greek,
        "hu" => Language::Hungarian,
        "it" => Language::Italian,
        "pt" => Language::Portuguese,
        "ro" => Language::Romanian,
        "ru" => Language::Russian,
        "es" => Language::Spanish,
        "sv" => Language::Swedish,
        "ta" => Language::Tamil,
        "tr" => Language::Turkish,
        _ => Language::English,
    }
}

pub fn index_glossaries(glossaries: &Glossaries) -> IndexedGlossaries {
    let mut schema = Schema::builder();
    let options = TextOptions::default().set_indexing_options(
        TextFieldIndexing::default()
            .set_tokenizer("tokenizer")
            .set_index_option(IndexRecordOption::WithFreqsAndPositions),
    );
    let title_field = schema.add_text_field("title", options.clone());
    let body_field = schema.add_text_field("body", options);
    let position_field = schema.add_facet_field("position");
    let schema = schema.build();

    let glossaries = glossaries.iter();
    let glossaries = glossaries.map(|(language, glossary)| {
        eprint!("Indexing `{}`... ", language);
        let _ = std::io::stderr().flush();

        let directory = Path::new("indexed").join(language);
        let _ = std::fs::remove_dir_all(&directory);
        std::fs::create_dir_all(&directory).unwrap();

        let index = Index::create_in_dir(&directory, schema.clone()).unwrap();

        let tokenizer = SimpleTokenizer
            .filter(LowerCaser)
            .filter(Stemmer::new(get_language(language)));

        index.tokenizers().register("tokenizer", tokenizer);

        let mut writer = index.writer(50 * 1024 * 1024).unwrap();

        let sections = glossary.sections.iter().chain(&glossary.variables);

        sections.for_each(|(location, section)| {
            let location = format!("/{}", location);
            let mut document = Document::new();
            document.add_text(title_field, &section.title);
            document.add_text(body_field, &section.description);
            document.add_facet(position_field, &location);

            writer.add_document(document);
        });

        writer.commit().unwrap();
        eprintln!("Done");

        (language.clone(), index)
    });
    let glossaries = glossaries.collect();

    IndexedGlossaries {
        title_field,
        body_field,
        position_field,
        glossaries,
    }
}
