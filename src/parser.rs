use attheme_glossary_parser::{
    glossary::{self, Contents, Glossary, ParagraphItem},
    parse as parse_glossary_dir,
};
use indexmap::IndexMap;
use std::{
    collections::HashMap,
    fs::DirEntry,
    io::{self, Write},
    path::{Path, PathBuf},
    sync::Arc,
};

mod index_renderer;
mod renderer;

use renderer::create_target;

pub type Glossaries = HashMap<String, GlossaryItems>;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct GlossaryItems {
    pub variables: HashMap<String, Section>,
    // The order is not importnt for variables as we only search through them,
    // but it _is_ important when showing glossary outline.
    pub sections: IndexMap<String, Section>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Section {
    pub title: String,
    /// The contents seen in sent messages
    pub contents: String,
    /// The description visible when searching the glossary
    pub description: String,
    pub preview: Option<Arc<Image>>,
    pub is_top_level: bool,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Image {
    pub url: String,
    pub description: String,
}

fn filter_map_dirs(entry: io::Result<DirEntry>) -> Option<PathBuf> {
    let entry = entry.ok()?;
    let file_type = entry.file_type().ok()?;

    if file_type.is_dir() {
        Some(entry.path())
    } else {
        None
    }
}

fn parse_glossary(path: &Path) -> Option<(String, Glossary)> {
    eprint!("Parsing `{}`... ", path.to_string_lossy());
    let _ = std::io::stderr().flush();

    let language = path.file_name().unwrap().to_string_lossy().into_owned();

    match parse_glossary_dir(path) {
        Ok(glossary) => {
            eprintln!("Done");

            Some((language, glossary))
        }
        Err(errors) => {
            eprintln!("Failed:");
            errors.iter().for_each(|error| eprintln!("{:#?}", error));

            None
        }
    }
}

fn extract_images<'a>(
    language: &str,
    images: &mut HashMap<&'a str, Arc<Image>>,
    content: &'a Contents,
) {
    match content {
        Contents::Image {
            url, description, ..
        } => {
            let url = format!(
                "https://attheme-glossary.snejugal.ru/{}/{}",
                language, url
            );

            let paragraph_items = description.items.iter();
            let variables = paragraph_items.filter_map(|item| match item {
                ParagraphItem::Monospace(variable) => Some(variable.as_ref()),
                _ => None,
            });

            let mut buffer = String::new();
            renderer::render_paragraph(
                &mut buffer,
                language,
                &description.items,
            );
            let image = Arc::new(Image {
                url,
                description: buffer,
            });

            variables.for_each(|variable| {
                images.insert(variable, Arc::clone(&image));
            });
        }
        Contents::Subsection(subsection) => {
            subsection.contents.iter().for_each(|contents| {
                extract_images(language, images, contents)
            });
        }
        _ => (),
    }
}

fn extract_variables(
    language: &str,
    images: &HashMap<&str, Arc<Image>>,
    variables: &mut HashMap<String, Section>,
    prefix: &str,
    content: &Contents,
) {
    match content {
        Contents::Subsection(subsection) => {
            let prefix =
                format!("{}{}/", prefix, create_target(&subsection.title));
            subsection.contents.iter().for_each(|content| {
                extract_variables(
                    language, images, variables, &prefix, content,
                );
            });
        }
        Contents::Variable {
            name, description, ..
        } => {
            let anchor = format!("{}{}", prefix, name);
            let contents = {
                let mut buffer = String::new();
                renderer::render_section(
                    &mut buffer,
                    language,
                    prefix,
                    name,
                    description,
                    false,
                );
                buffer
            };
            let description = {
                let mut buffer = String::new();
                index_renderer::render_contents(
                    &mut buffer,
                    language,
                    description,
                );
                buffer
            };
            let variable = Section {
                title: name.clone(),
                preview: images.get(name.as_str()).map(Arc::clone),
                is_top_level: false,
                contents,
                description,
            };
            variables.insert(anchor, variable);
        }
        _ => (),
    }
}

fn collect_variables(
    language: &str,
    glossary: &Glossary,
) -> HashMap<String, Section> {
    let mut images = HashMap::new();
    let sections = glossary.sections.iter();
    let contents = sections.map(|x| &x.section.contents).flatten();
    contents.for_each(|content| extract_images(language, &mut images, content));

    // We can't extract images and variables at the same time, because
    // images always go after the variables they preview. So we first
    // extract images, then we extract variables.
    let mut variables = HashMap::new();
    let sections = glossary.sections.iter().map(|x| &x.section);
    sections.for_each(|section| {
        let prefix = &format!("{}/", create_target(&section.title));
        section.contents.iter().for_each(|content| {
            extract_variables(
                language,
                &images,
                &mut variables,
                prefix,
                content,
            )
        })
    });

    variables
}

fn find_preview(language: &str, contents: &[Contents]) -> Option<Arc<Image>> {
    contents.iter().find_map(|content| match content {
        Contents::Image { url, .. } => Some(Arc::new(Image {
            url: format!(
                "https://attheme-glossary.snejugal.ru/{}/{}",
                language, url
            ),
            description: String::new(), // won't be used anyway
        })),
        Contents::Subsection(subsection) => {
            find_preview(language, &subsection.contents)
        }
        _ => None,
    })
}

fn extract_sections(
    language: &str,
    sections: &mut IndexMap<String, Section>,
    prefix: &str,
    is_first_section: bool,
    is_top_level: bool,
    raw_section: &glossary::Section,
) {
    let section = Section {
        title: raw_section.title.clone(),
        contents: {
            let mut buffer = String::new();
            renderer::render_section(
                &mut buffer,
                language,
                prefix,
                &raw_section.title,
                &raw_section.contents,
                is_first_section && is_top_level,
            );
            buffer
        },
        description: {
            let mut buffer = String::new();
            index_renderer::render_contents(
                &mut buffer,
                language,
                &raw_section.contents,
            );
            buffer
        },
        preview: find_preview(language, &raw_section.contents),
        is_top_level,
    };
    let prefix = format!("{}{}/", prefix, create_target(&raw_section.title));
    sections.insert(prefix.clone(), section);

    let contents = raw_section.contents.iter();
    contents.for_each(|content| {
        if let Contents::Subsection(subsection) = content {
            extract_sections(
                language, sections, &prefix, false, false, subsection,
            );
        }
    });
}

fn collect_sections(
    language: &str,
    glossary: &Glossary,
) -> IndexMap<String, Section> {
    let mut sections = IndexMap::new();

    let raw_sections = glossary.sections.iter().map(|x| &x.section);
    raw_sections.enumerate().for_each(|(index, section)| {
        extract_sections(
            language,
            &mut sections,
            "",
            index == 0,
            true,
            section,
        );
    });

    sections
}

fn process_glossary(
    (language, glossary): (String, Glossary),
) -> (String, GlossaryItems) {
    let variables = collect_variables(&language, &glossary);
    let sections = collect_sections(&language, &glossary);

    let glossary = GlossaryItems {
        variables,
        sections,
    };

    (language, glossary)
}

pub fn parse() -> Glossaries {
    let database =
        std::fs::read_dir("database").expect("No `database` directory");
    let glossary_dirs = database.filter_map(filter_map_dirs);

    let mut has_errored = false;
    let glossaries: Vec<_> = glossary_dirs
        .filter_map(|path| {
            let glossary = parse_glossary(&path);

            if glossary.is_none() {
                has_errored = true;
            }

            glossary
        })
        .collect();

    if has_errored {
        std::process::exit(1);
    }

    glossaries.into_iter().map(process_glossary).collect()
}
