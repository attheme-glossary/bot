use crate::{Glossaries, IndexedGlossaries};
use std::sync::Arc;
use tantivy::{collector::TopDocs, query::QueryParser, schema::Value};
use tbot::{
    connectors::Connector,
    contexts::Inline,
    types::{
        inline_query::{
            self,
            result::{Article, Thumb},
        },
        input_message_content::Text,
        parameters::Text as ParseMode,
        User,
    },
};

const CACHE_TIME: u64 = 30 * 60;
const ICON: &str = "https://attheme-glossary.snejugal.ru/static/social.jpg";

fn get_language<'a>(
    forced_language: Option<&'a str>,
    user: &'a User,
    glossaries: &'a Glossaries,
) -> &'a str {
    user.language_code
        .as_ref()
        .map(|language_code| {
            if let Some(language) = forced_language {
                language
            } else {
                language_code.get(0..2).unwrap_or("")
            }
        })
        .and_then(|language| {
            if glossaries.contains_key(language) {
                Some(language)
            } else {
                None
            }
        })
        .unwrap_or("en")
}

async fn show_sections<C>(
    language: &str,
    context: &Inline<C>,
    glossaries: &Glossaries,
) where
    C: Connector,
{
    let glossary = glossaries.get(language).unwrap();
    let sections = glossary
        .sections
        .iter()
        .filter_map(|(path, section)| {
            if section.is_top_level {
                let id = format!("{:x}", md5::compute(path));
                Some((id, section))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    let sections = sections
        .iter()
        .map(|(id, section)| {
            let content = Text::new(ParseMode::markdown(&section.contents));
            let thumb = section
                .preview
                .as_ref()
                .map(|x| x.url.as_ref())
                .unwrap_or(ICON);
            let article =
                Article::new(&section.title, content).thumb(Thumb::new(thumb));

            inline_query::Result::new(&id, article)
        })
        .collect::<Vec<_>>();

    let result = context
        .answer(&sections)
        .cache_time(CACHE_TIME)
        .call()
        .await;

    if let Err(err) = result {
        dbg!(err);
    }
}

fn search_for_sections(
    language: &str,
    indexed_glossaries: &IndexedGlossaries,
    query: &str,
) -> Vec<String> {
    let index = indexed_glossaries.glossaries.get(language).unwrap();
    let reader = index.reader().unwrap();
    let searcher = reader.searcher();

    let query_parser = QueryParser::for_index(
        index,
        vec![
            indexed_glossaries.title_field,
            indexed_glossaries.body_field,
        ],
    );

    let query = query_parser.parse_query(query).unwrap();
    let results = searcher.search(&query, &TopDocs::with_limit(50)).unwrap();

    let positions = results.into_iter().filter_map(|(_, doc_address)| {
        let doc = searcher.doc(doc_address).ok()?;

        let position = match doc.get_first(indexed_glossaries.position_field)? {
            Value::Facet(position) => position.encoded_str(),
            _ => return None,
        };

        Some(position.replace('\0', "/"))
    });

    positions.collect()
}

async fn search_glossary<C>(
    language: &str,
    context: &Inline<C>,
    glossaries: &Glossaries,
    indexed_glossaries: &IndexedGlossaries,
    query: &str,
) where
    C: Connector,
{
    let glossary = glossaries.get(language).unwrap();

    let results = search_for_sections(language, indexed_glossaries, query);
    let sections = results.into_iter().filter_map(|position| {
        let id = format!("{:x}", md5::compute(&position));

        let section = glossary
            .sections
            .get(&position)
            .or_else(|| glossary.variables.get(&position))?;

        Some((id, section))
    });
    let sections = sections.collect::<Vec<_>>();

    let results = sections.iter().map(|(id, section)| {
        let content = Text::new(ParseMode::markdown(&section.contents));
        let thumb = section
            .preview
            .as_ref()
            .map(|x| x.url.as_ref())
            .unwrap_or(ICON);
        let article = Article::new(&section.title, content)
            .description(&section.description)
            .thumb(Thumb::new(thumb));

        inline_query::Result::new(&id, article)
    });
    let results = results.collect::<Vec<_>>();

    let result = context.answer(&results).cache_time(CACHE_TIME).call().await;

    if let Err(err) = result {
        dbg!(err);
    }
}

async fn inline<C>(
    context: Arc<Inline<C>>,
    glossaries: Arc<Glossaries>,
    indexed_glossaries: Arc<IndexedGlossaries>,
) where
    C: Connector,
{
    let forced_language =
        context
            .query
            .split_whitespace()
            .nth(0)
            .and_then(|language| {
                if language.starts_with("--") && language.len() == 4 {
                    language.get(2..)
                } else {
                    None
                }
            });

    let query = if forced_language.is_some() {
        context.query.get(5..).unwrap_or("")
    } else {
        &context.query
    };

    let language = get_language(forced_language, &context.from, &glossaries);

    if query.is_empty() {
        show_sections(language, &context, &glossaries).await;
    } else {
        search_glossary(
            language,
            &context,
            &glossaries,
            &indexed_glossaries,
            query,
        )
        .await;
    }
}

pub async fn run(
    glossaries: Glossaries,
    indexed_glossaries: IndexedGlossaries,
) {
    let glossaries = Arc::new(glossaries);
    let indexed_glossaries = Arc::new(indexed_glossaries);
    let mut bot = tbot::from_env!("BOT_TOKEN").event_loop();

    let glossaries_inline = Arc::clone(&glossaries);
    let indexed_glossaries_inline = Arc::clone(&indexed_glossaries);
    bot.inline(move |context| {
        inline(
            context,
            Arc::clone(&glossaries_inline),
            Arc::clone(&indexed_glossaries_inline),
        )
    });

    eprintln!("Started the bot.");
    bot.polling().start().await.unwrap();
}
